import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'dart:async';

class ArticlePage extends StatefulWidget {
  final String blogUrl;
  ArticlePage({this.blogUrl});
  @override
  _ArticlePageState createState() => _ArticlePageState();
}

class _ArticlePageState extends State<ArticlePage> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Akandi"),
            Text("News", style: TextStyle(color: Colors.greenAccent))
          ],
        ),
        elevation: 0.0,
        centerTitle: true,
      ),
      body: Container(
          child: WebView(
              initialUrl: widget.blogUrl,
              onWebViewCreated: (WebViewController webViewController) {
                _controller.complete(webViewController);
              })),
    );
  }
}
