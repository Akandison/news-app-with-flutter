import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:newsApp/Models/BlogModel.dart';
import 'package:newsApp/Models/categoriesModel.dart';
import 'package:newsApp/Resources/data.dart';
import 'package:newsApp/Resources/new.dart';
import 'package:newsApp/views/article.dart';
import 'package:newsApp/views/categories.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<CategoryModel> category = new List<CategoryModel>();
  List<BlogModel> articles = new List<BlogModel>();
  bool _loading = true;
  @override
  void initState() {
    super.initState();
    category = categories();
    getNews();
  }

  getNews() async {
    News newsObject = News();
    await newsObject.getNews();
    articles = newsObject.news;
    setState(() {
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Akandi"),
            Text("News", style: TextStyle(color: Colors.greenAccent))
          ],
        ),
        elevation: 0.0,
        centerTitle: true,
      ),
      body: _loading
          ? Center(
              child: Container(child: CircularProgressIndicator()),
            )
          : SingleChildScrollView(
              child: Container(
                child: Column(children: <Widget>[
                  //categories
                  Container(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      height: 70,
                      child: ListView.builder(
                          itemCount: category.length,
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return CategoryTile(
                              imageUrl: category[index].imageUrl,
                              categoryName: category[index].categoryName,
                            );
                          })),
                  //Blogs
                  Container(
                    padding: EdgeInsets.only(top: 16),
                    margin: EdgeInsets.only(top: 16),
                    child: ListView.builder(
                        physics: ClampingScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: articles.length,
                        itemBuilder: (context, index) {
                          return BlogTile(
                              imageUrl: articles[index].urlToImage,
                              title: articles[index].title,
                              desc: articles[index].description,
                              url: articles[index].url);
                        }),
                  )
                ]),
              ),
            ),
      drawer: Drawer(
        child: ListView(
          padding: const EdgeInsets.all(0),
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text("Constantinople Money ventures"),
              accountEmail: Text("adomahconstance@gmail.com"),
              currentAccountPicture: CircleAvatar(
                  backgroundImage:
                      AssetImage("images/FB_IMG_1594306515846.jpg")),
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text("Quality business at a go"),
              subtitle: Text("Ready to partner leading companies"),
              trailing: Icon(Icons.phone_bluetooth_speaker),
            ),
            ListTile(
              leading: Icon(Icons.book),
              title: Text("Money transfers with memories"),
              subtitle: Text("A shoulder you can rely on: Lets do business"),
              trailing: Icon(Icons.business),
            )
          ],
        ),
      ),
    );
  }
}

class CategoryTile extends StatelessWidget {
  final String imageUrl, categoryName;
  CategoryTile({this.imageUrl, this.categoryName});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => CategoriesPage(
                      category: categoryName.toLowerCase(),
                    )));
      },
      child: Container(
        margin: EdgeInsets.only(right: 16),
        child: Stack(
          children: <Widget>[
            ClipRRect(
                borderRadius: BorderRadius.circular(6),
                child: CachedNetworkImage(
                    imageUrl: imageUrl,
                    width: 120,
                    height: 60,
                    fit: BoxFit.cover)),
            Container(
                alignment: Alignment.center,
                width: 120,
                height: 60,
                decoration: BoxDecoration(
                    color: Colors.black26,
                    borderRadius: BorderRadius.circular(6)),
                child: Text(categoryName,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.bold)))
          ],
        ),
      ),
    );
  }
}

class BlogTile extends StatelessWidget {
  final String imageUrl, title, desc, url;
  BlogTile(
      {@required this.imageUrl,
      @required this.title,
      @required this.desc,
      @required this.url});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => ArticlePage(blogUrl: url)));
      },
      child: Container(
          margin: EdgeInsets.only(bottom: 16),
          child: Column(children: <Widget>[
            ClipRRect(
                borderRadius: BorderRadius.circular(6),
                child: Image.network(imageUrl)),
            SizedBox(
              height: 8,
            ),
            Text(title,
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.black87,
                    fontWeight: FontWeight.w600)),
            SizedBox(
              height: 8,
            ),
            Text(
              desc,
              style: TextStyle(color: Colors.black54),
            )
          ])),
    );
  }
}
