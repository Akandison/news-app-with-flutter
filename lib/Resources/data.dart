import 'package:newsApp/Models/categoriesModel.dart';

List<CategoryModel> categories() {
  List<CategoryModel> categories = List<CategoryModel>();
  CategoryModel categoriesObject = CategoryModel();
//1
  categoriesObject = CategoryModel();
  categoriesObject.categoryName = "Business";
  categoriesObject.imageUrl =
      "https://images.unsplash.com/photo-1576267423048-15c0040fec78?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80";
  categories.add(categoriesObject);

//2
  categoriesObject = CategoryModel();
  categoriesObject.categoryName = "Politics";
  categoriesObject.imageUrl =
      "https://images.unsplash.com/photo-1502772066658-3006ff41449b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1456&q=80";
  categories.add(categoriesObject);

//3
  categoriesObject = CategoryModel();
  categoriesObject.categoryName = "Health";
  categoriesObject.imageUrl =
      "https://images.unsplash.com/photo-1532938911079-1b06ac7ceec7?ixlib=rb-1.2.1&auto=format&fit=crop&w=889&q=80";
  categories.add(categoriesObject);
//4
  categoriesObject = CategoryModel();
  categoriesObject.categoryName = "Sports";
  categoriesObject.imageUrl =
      "https://images.unsplash.com/photo-1541252260730-0412e8e2108e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=322&q=80";
  categories.add(categoriesObject);
//5
  categoriesObject = CategoryModel();
  categoriesObject.categoryName = "Entertainment";
  categoriesObject.imageUrl =
      "https://images.unsplash.com/photo-1486572788966-cfd3df1f5b42?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=752&q=80";
  categories.add(categoriesObject);

  return categories;
}
