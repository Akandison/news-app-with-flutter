import 'dart:convert';

import 'package:newsApp/Models/BlogModel.dart';
import 'package:http/http.dart' as http;

class News {
  List<BlogModel> news = [];
  Future<void> getNews() async {
    String url =
        "http://newsapi.org/v2/top-headlines?country=us&apiKey=db47f484410343629b285928b4790839";
    var response = await http.get(url);
    var newsjsonData = await jsonDecode(response.body);
    if (newsjsonData["status"] == "ok") {
      newsjsonData["articles"].forEach((element) {
        if (element["urlToImage"] != null && element["description"] != null) {
          BlogModel articles = BlogModel(
              title: element["title"],
              author: element["author"],
              description: element["description"],
              url: element["url"],
              urlToImage: element["urlToImage"],
              content: element["element"]);
          news.add(articles);
        }
      });
    }
  }
}

class CategoryNewsClass {
  List<BlogModel> news = [];
  Future<void> getNews(String category) async {
    String url =
        "http://newsapi.org/v2/top-headlines?category=$category&country=us&apiKey=db47f484410343629b285928b4790839";
    // "http://newsapi.org/v2/top-headlines?country=in&category=$category&apiKey=db47f484410343629b285928b4790839"
    var response = await http.get(url);
    var newsjsonData = await jsonDecode(response.body);
    if (newsjsonData["status"] == "ok") {
      newsjsonData["articles"].forEach((element) {
        if (element["urlToImage"] != null && element["description"] != null) {
          BlogModel articles = BlogModel(
              title: element["title"],
              author: element["author"],
              description: element["description"],
              url: element["url"],
              urlToImage: element["urlToImage"],
              content: element["element"]);
          news.add(articles);
        }
      });
    }
  }
}
